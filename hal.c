
/*
 * ======== hal.c ========
 *
 */
#include "msp430.h"

#include "driverlib.h"

#include "USB_API/USB_Common/device.h"
#include "USB_config/descriptors.h"

#include "hal.h"

#define GPIO_ALL	GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3| \
					GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7

extern volatile uint8_t button1Pressed;
extern volatile uint8_t button2Pressed;
extern volatile uint8_t button3Pressed;
extern volatile uint8_t button4Pressed;
extern volatile uint8_t button5Pressed;
extern volatile uint8_t button6Pressed;
extern volatile uint8_t joykey1,joykey2,joykey3,joykey4,joykey5,joykey6;
extern volatile unsigned int result0[4];
extern volatile unsigned int result1[4];
extern volatile unsigned int result2[4];
extern volatile unsigned int result3[4];

/*
 * This function drives all the I/O's as output-low, to avoid floating inputs
 * (which cause extra power to be consumed).  This setting is compatible with  
 * TI FET target boards, the F5529 Launchpad, and F5529 Experimenters Board;  
 * but may not be compatible with custom hardware, which may have components  
 * attached to the I/Os that could be affected by these settings.  So if using
 * other boards, this function may need to be modified.
 */
void USBHAL_initPorts(void)
{
#ifdef __MSP430_HAS_PORT1_R__
    GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_ALL);
    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_ALL);
#endif

#ifdef __MSP430_HAS_PORT2_R__
    GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_ALL);
    GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_ALL);
#endif

#ifdef __MSP430_HAS_PORT3_R__
    GPIO_setOutputLowOnPin(GPIO_PORT_P3, GPIO_ALL);
    GPIO_setAsOutputPin(GPIO_PORT_P3, GPIO_ALL);
#endif

#ifdef __MSP430_HAS_PORT4_R__
    GPIO_setOutputLowOnPin(GPIO_PORT_P4, GPIO_ALL);
    GPIO_setAsOutputPin(GPIO_PORT_P4, GPIO_ALL);
#endif

#ifdef __MSP430_HAS_PORT5_R__
    GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_ALL);
    GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_ALL);
#endif

#ifdef __MSP430_HAS_PORT6_R__
    GPIO_setOutputLowOnPin(GPIO_PORT_P6, GPIO_ALL);
    GPIO_setAsOutputPin(GPIO_PORT_P6, GPIO_ALL);
#endif

#ifdef __MSP430_HAS_PORT7_R__
    GPIO_setOutputLowOnPin(GPIO_PORT_P7, GPIO_ALL);
    GPIO_setAsOutputPin(GPIO_PORT_P7, GPIO_ALL);
#endif

#ifdef __MSP430_HAS_PORT8_R__
    GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_ALL);
    GPIO_setAsOutputPin(GPIO_PORT_P8, GPIO_ALL);
#endif

#ifdef __MSP430_HAS_PORT9_R__
    GPIO_setOutputLowOnPin(GPIO_PORT_P9, GPIO_ALL);
    GPIO_setAsOutputPin(GPIO_PORT_P9, GPIO_ALL);
#endif

#ifdef __MSP430_HAS_PORTJ_R__
    GPIO_setOutputLowOnPin(GPIO_PORT_PJ, GPIO_ALL);
    GPIO_setAsOutputPin(GPIO_PORT_PJ, GPIO_ALL);
#endif
}

/* Configures the system clocks:
* MCLK = SMCLK = DCO/FLL = mclkFreq (expected to be expressed in Hz)
* ACLK = FLLref = REFO=32kHz
*
* XT2 is not configured here.  Instead, the USB API automatically starts XT2
* when beginning USB communication, and optionally disables it during USB
* suspend.  It's left running after the USB host is disconnected, at which
* point you're free to disable it.  You need to configure the XT2 frequency
* in the Descriptor Tool (currently set to 4MHz in this example).
* See the Programmer's Guide for more information.
*/

void USBHAL_initClocks(uint32_t mclkFreq)
{
	UCS_initClockSignal(
	   UCS_FLLREF,
	   UCS_REFOCLK_SELECT,
	   UCS_CLOCK_DIVIDER_1);

	UCS_initClockSignal(
	   UCS_ACLK,
	   UCS_REFOCLK_SELECT,
	   UCS_CLOCK_DIVIDER_1);

    UCS_initFLLSettle(
        mclkFreq/1000,
        mclkFreq/32768);
}

void ADC_init(void)
{
	P6SEL = 0x0F;                             // Enable A/D channel inputs
	ADC12CTL0 = ADC12ON+ADC12MSC+ADC12SHT0_4; // Turn on ADC12, set sampling time
	ADC12CTL1 = ADC12SHP+ADC12CONSEQ_1;       // Use sampling timer, single sequence
	ADC12MCTL3 = ADC12INCH_3;                 // ref+=AVcc, channel = A3
	ADC12MCTL4 = ADC12INCH_4;					// ref+=AVcc, channel = A4
	ADC12MCTL5 = ADC12INCH_5 + ADC12EOS;		// ref+=AVcc, channel = A5 , end seq
	ADC12IE |= 0x20;                           // Enable ADC12IFG.3,4,5
	ADC12CTL0 |= ADC12ENC;					// Enable conversion
}

void USBHAL_initButtons(void)
{
	// P1.1 TAB key
	GPIO_setOutputHighOnPin(BUTTON1_PORT, BUTTON1_PIN);
	GPIO_setAsInputPinWithPullUpResistor(BUTTON1_PORT, BUTTON1_PIN);
	GPIO_clearInterrupt(BUTTON1_PORT, BUTTON1_PIN);
	GPIO_enableInterrupt(BUTTON1_PORT, BUTTON1_PIN);

	//P1.3 CTRL + Z undo
	GPIO_setOutputHighOnPin(BUTTON3_PORT, BUTTON3_PIN);
	GPIO_setAsInputPinWithPullUpResistor(BUTTON3_PORT, BUTTON3_PIN);
	GPIO_clearInterrupt(BUTTON3_PORT, BUTTON3_PIN);
	GPIO_enableInterrupt(BUTTON3_PORT, BUTTON3_PIN);

	// P1.4 CRTL + Y redo
	GPIO_setOutputHighOnPin(BUTTON5_PORT, BUTTON5_PIN);
	GPIO_setAsInputPinWithPullUpResistor(BUTTON5_PORT, BUTTON5_PIN);
	GPIO_clearInterrupt(BUTTON5_PORT, BUTTON5_PIN);
	GPIO_enableInterrupt(BUTTON5_PORT, BUTTON5_PIN);

	//p1.5 FIT ALL IN
	GPIO_setOutputHighOnPin(BUTTON6_PORT,BUTTON6_PIN);
	GPIO_setAsInputPinWithPullUpResistor(BUTTON6_PORT, BUTTON6_PIN);
	GPIO_clearInterrupt(BUTTON6_PORT, BUTTON6_PIN);
	GPIO_enableInterrupt(BUTTON6_PORT, BUTTON6_PIN);

	//P2.1 SHIFT
	GPIO_setOutputHighOnPin(BUTTON2_PORT, BUTTON2_PIN);
	GPIO_setAsInputPinWithPullUpResistor(BUTTON2_PORT, BUTTON2_PIN);
	GPIO_clearInterrupt(BUTTON2_PORT, BUTTON2_PIN);
	//GPIO_enableInterrupt(BUTTON2_PORT, BUTTON2_PIN);

	//P2.2
	GPIO_setOutputHighOnPin(BUTTON4_PORT, BUTTON4_PIN);
	GPIO_setAsInputPinWithPullUpResistor(BUTTON4_PORT, BUTTON4_PIN);
	GPIO_clearInterrupt(BUTTON4_PORT, BUTTON4_PIN);
	/*P1REN |= 0x02;
	P1OUT |= 0x02;
	P1IFG &= ~(0x02);
	P1IE |= 0x02;

	P1REN |= 0x04;
	P1OUT |= 0x04;
	P1IFG &= ~(0x04);
	P1IE |= 0x04;

	P2REN |= 0x02;
	P2OUT |= 0x02;*/
}

//Released_Version_5_10_00_17
