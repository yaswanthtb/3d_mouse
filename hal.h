/*
 * ======== hal.h ========
 *
 * Device and board specific pins need to be configured here
 *
 */

// Only one of the following board defines should be enabled!
#define MSP430F5529LP       // F5529 LaunchPad
//#define MSPEXP430F5529     // F5529 Experimenters Board
//#define MSPTS430PN80USB    // FET target board
//#define MSPTS430RGC64USB   // FET target board
//#define MSPTS430PZ100USB   // FET target board


#ifdef MSPEXP430F5529
#define BUTTON1_PORT	GPIO_PORT_P1
#define BUTTON1_PIN		GPIO_PIN7
#define BUTTON1_VECTOR	PORT1_VECTOR
#define INTERRUPT_PIN   GPIO_PIN6

#define BUTTON2_PORT	GPIO_PORT_P2
#define BUTTON2_PIN		GPIO_PIN2
#define BUTTON2_VECTOR	PORT2_VECTOR
#endif

#if defined (MSPTS430RGC64USB) || defined (MSPTS430PN80USB)  || defined (MSPTS430PZ100USB)
#define BUTTON1_PORT	GPIO_PORT_P1
#define BUTTON1_PIN		GPIO_PIN6
#define BUTTON1_VECTOR	PORT1_VECTOR
#define INTERRUPT_PIN   GPIO_PIN1

#define BUTTON2_PORT	GPIO_PORT_P1
#define BUTTON2_PIN		GPIO_PIN7
#define BUTTON2_VECTOR	PORT1_VECTOR
#endif

#ifdef MSP430F5529LP
#define BUTTON1_PORT	GPIO_PORT_P1
#define BUTTON1_PIN		GPIO_PIN1
#define BUTTON1_VECTOR	PORT1_VECTOR
#define INTERRUPT_PIN   GPIO_PIN6

// crtl+z
#define BUTTON3_PORT	GPIO_PORT_P1
#define BUTTON3_PIN		GPIO_PIN3
#define BUTTON3_VECTOR	PORT1_VECTOR
#define INTERRUPT_PIN   GPIO_PIN6

//SHIFT
#define BUTTON2_PORT	GPIO_PORT_P2
#define BUTTON2_PIN		GPIO_PIN1
#define BUTTON2_VECTOR	PORT2_VECTOR

//CTRL
#define BUTTON4_PORT	GPIO_PORT_P2
#define BUTTON4_PIN		GPIO_PIN2
#define BUTTON4_VECTOR	PORT2_VECTOR

// ctrl + y
#define BUTTON5_PORT	GPIO_PORT_P1
#define BUTTON5_PIN		GPIO_PIN4
#define BUTTON5_VECTOR	PORT1_VECTOR
#define INTERRUPT_PIN	GPIO_PIN6

//fit all in
#define BUTTON6_PORT	GPIO_PORT_P1
#define BUTTON6_PIN		GPIO_PIN5
#define BUTTON6_VECTOR	PORT1_VECTOR
#define INTERRUPT_PIN	GPIO_PIN6

#endif


/*----------------------------------------------------------------------------
 * The following function names are deprecated.  These were updated to new 
 * names to follow OneMCU naming convention.
 +---------------------------------------------------------------------------*/

#ifndef DEPRECATED
#define   initPorts       USBHAL_initPorts
#define   initClocks      USBHAL_initClocks
#define   initButtons     USBHAL_initButtons
#endif

void USBHAL_initPorts(void);
void USBHAL_initClocks(uint32_t mclkFreq);
void USBHAL_initButtons(void);
void ADC_init(void);
//Released_Version_5_10_00_17
