#include <string.h>

#include "driverlib.h"

#include "USB_config/descriptors.h"
#include "USB_API/USB_Common/device.h"
#include "USB_API/USB_Common/usb.h"                  // USB-specific functions
#include "USB_API/USB_HID_API/UsbHid.h"

#include "USB_app/keyboard.h"
/*
 * NOTE: Modify hal.h to select a specific evaluation board and customize for
 * your own board.
 */
#include "hal.h"

//delay function
void delay (long n);
void delay(long n)
{
	volatile int i;
	for (i=0; i>=n; i++)
	{

	}
	return;
}

/*********** Application specific globals **********************/
volatile unsigned int result0[4];
volatile unsigned int result1[4];
volatile unsigned int result2[4];
volatile unsigned int result3[4];
volatile uint8_t joykey1= FALSE;
volatile uint8_t joykey2= FALSE;
volatile uint8_t joykey3= FALSE;
volatile uint8_t joykey4= FALSE;
volatile uint8_t joykey5= FALSE;
volatile uint8_t joykey6= FALSE;
volatile uint8_t button1Pressed = FALSE;
volatile uint8_t button2Pressed = FALSE;
volatile uint8_t button3Pressed = FALSE;
volatile uint8_t button4Pressed = FALSE;
volatile uint8_t button5Pressed = FALSE;
volatile uint8_t button6Pressed = FALSE;
volatile uint8_t keySendComplete = TRUE;
uint8_t buttonzBuf[10] = {z, 0x00};
//uint8_t button6Buf[10] = {};
uint8_t button1Buf[10] = {KEY_TAB, 0x00};
uint8_t buttonupBuf[10] = {KEY_UP_ARROW, 0x00};
uint8_t buttondownBuf[10] = {KEY_DOWN_ARROW, 0x00};
uint8_t buttonleftBuf[10] = {KEY_LEFT_ARROW, 0x00};
uint8_t buttonrightBuf[10] = {KEY_RIGHT_ARROW, 0x00};
uint8_t button1StringLength;
uint8_t button2StringLength;
uint8_t button3StringLength;
uint8_t button4StringLength;
uint8_t button5StringLength;
uint8_t button6StringLength;
uint8_t button7StringLength;
uint8_t button8StringLength;

/*  
* ======== main ========
*/

void main (void)
{
	uint8_t i;
    uint8_t j;
    uint8_t k;

     WDT_A_hold(WDT_A_BASE); // Stop watchdog timer

    // Minumum Vcore setting required for the USB API is PMM_CORE_LEVEL_2 .
    PMM_setVCore(PMM_CORE_LEVEL_2);
    USBHAL_initPorts();           // Config GPIOS for low-power (output low)
    USBHAL_initClocks(8000000);   // Config clocks. MCLK=SMCLK=FLL=8MHz; ACLK=REFO=32kHz
    USBHAL_initButtons();         // Init the two buttons
    ADC_init(); // init ADC
    Keyboard_init();       // Init keyboard report
    USB_setup(TRUE, TRUE); // Init USB & events; if a host is present, connect

    __enable_interrupt();  // Enable global interrupts

    while (1)
    {
        switch(USB_getConnectionState())
        {
            // This case is executed while your device is enumerated on the
            // USB host
            case ST_ENUM_ACTIVE:
              ADC12CTL0 &= ~(ADC12ENC);
          	  ADC12CTL0 |= ADC12ENC + ADC12SC;		//Enable and start convertion
                // Enter LPM0 w/interrupt, until a keypress occurs
          	  ADC12IE &= ~(0x01); // clear AdC12IE0
          	  __bis_SR_register(LPM0_bits + GIE);

                /************* HID keyboard portion ************************/
          	  // TAB and Shift keys
                if (button1Pressed){
                    button1StringLength = strlen((const char *)button1Buf);
                    if (button2Pressed) {
                        Keyboard_press(KEY_LEFT_SHIFT);
                        while(!keySendComplete);
                        keySendComplete = FALSE;
                    }
                    for (i=0; i<button1StringLength; i++) {
                    	Keyboard_press(button1Buf[i]);
                    	while(!keySendComplete);
                        keySendComplete = FALSE;
                        Keyboard_release(button1Buf[i]);
                        while(!keySendComplete);
                        keySendComplete = FALSE;
                    }
                    Keyboard_release(KEY_LEFT_SHIFT);
                    while(!keySendComplete);
                    keySendComplete = FALSE;
                    button1Pressed = FALSE;
                    button2Pressed = FALSE;
                }

                // combination of key in single button CTRL + Z
                if (button3Pressed){
                    	button2StringLength = strlen((const char *)buttonzBuf);
                        Keyboard_press(KEY_LEFT_CTRL);
                        while(!keySendComplete);
                        keySendComplete = FALSE;
                    for (i=0; i<button2StringLength; i++) {
                    	Keyboard_press(buttonzBuf[i]);
                    	while(!keySendComplete);
                        keySendComplete = FALSE;
                        Keyboard_release(buttonzBuf[i]);
                        while(!keySendComplete);
                        keySendComplete = FALSE;
                    }
                    Keyboard_release(KEY_LEFT_CTRL);
                    while(!keySendComplete);
                    keySendComplete = FALSE;
                	button3Pressed = FALSE;
                }


                /*********joystick keys portion ********/
                				if (joykey1){
                					button3StringLength = strlen((const char *)buttonupBuf);
                					Keyboard_press(KEY_RIGHT_CTRL);
                					while(!keySendComplete);
                					keySendComplete = FALSE;
                                    for (j=0; j<button3StringLength; j++) {
                                        Keyboard_press(buttonupBuf[j]);
                                        while(!keySendComplete);
                                        keySendComplete = FALSE;
                                        Keyboard_release(buttonupBuf[j]);
                                        while(!keySendComplete);
                                        keySendComplete = FALSE;
                                    }
                                    Keyboard_release(KEY_RIGHT_CTRL);
                                    while(!keySendComplete);
                                    keySendComplete = FALSE;
                                    joykey1 = FALSE;
                                }

                  				if (joykey2){
                    					button4StringLength = strlen((const char *)buttondownBuf);
                    					Keyboard_press(KEY_RIGHT_CTRL);
                    					while(!keySendComplete);
                    					keySendComplete = FALSE;
                                        for (j=0; j<button4StringLength; j++) {
                                            Keyboard_press(buttondownBuf[j]);
                                            while(!keySendComplete);
                                            keySendComplete = FALSE;
                                            Keyboard_release(buttondownBuf[j]);
                                            while(!keySendComplete);
                                            keySendComplete = FALSE;
                                        }
                                        Keyboard_release(KEY_RIGHT_CTRL);
                                        while(!keySendComplete);
                                        keySendComplete = FALSE;
                                    	joykey2 = FALSE;
                                    }

                				if (joykey3){
                					button5StringLength = strlen((const char *)buttonleftBuf);
                					Keyboard_press(KEY_RIGHT_CTRL);
                					while(!keySendComplete);
                					keySendComplete = FALSE;
                                    for (k=0; k<button5StringLength; k++) {
                                        Keyboard_press(buttonleftBuf[k]);
                                        while(!keySendComplete);
                                        keySendComplete = FALSE;
                                        Keyboard_release(buttonleftBuf[k]);
                                        while(!keySendComplete);
                                        keySendComplete = FALSE;
                                    }
                                    Keyboard_release(KEY_RIGHT_CTRL);
                                    while(!keySendComplete);
                                    keySendComplete = FALSE;
                                    joykey3 = FALSE;
                                }

                  				if (joykey4){
                    					button6StringLength = strlen((const char *)buttonrightBuf);
                    					Keyboard_press(KEY_RIGHT_CTRL);
                    					while(!keySendComplete);
                    					keySendComplete = FALSE;
                                        for (k=0; k<button6StringLength; k++) {
                                            Keyboard_press(buttonrightBuf[k]);
                                            while(!keySendComplete);
                                            keySendComplete = FALSE;
                                            Keyboard_release(buttonrightBuf[k]);
                                            while(!keySendComplete);
                                            keySendComplete = FALSE;
                                        }
                                        Keyboard_release(KEY_RIGHT_CTRL);
                                        while(!keySendComplete);
                                        keySendComplete = FALSE;
                                    	joykey4 = FALSE;
                                    }

                  				if (joykey5){
                    					button7StringLength = strlen((const char *)buttonrightBuf);
                    					Keyboard_press(KEY_RIGHT_SHIFT);
                    					while(!keySendComplete);
                    					keySendComplete = FALSE;
                                        for (k=0; k<button7StringLength; k++) {
                                            Keyboard_press(buttonrightBuf[k]);
                                            while(!keySendComplete);
                                            keySendComplete = FALSE;
                                            Keyboard_release(buttonrightBuf[k]);
                                            while(!keySendComplete);
                                            keySendComplete = FALSE;
                                        }
                                        Keyboard_release(KEY_RIGHT_SHIFT);
                                        while(!keySendComplete);
                                        keySendComplete = FALSE;
                                    	joykey5 = FALSE;
                                    }

                  				if (joykey6){
                    					button8StringLength = strlen((const char *)buttonleftBuf);
                    					Keyboard_press(KEY_RIGHT_SHIFT);
                    					while(!keySendComplete);
                    					keySendComplete = FALSE;
                                        for (k=0; k<button8StringLength; k++) {
                                            Keyboard_press(buttonleftBuf[k]);
                                            while(!keySendComplete);
                                            keySendComplete = FALSE;
                                            Keyboard_release(buttonleftBuf[k]);
                                            while(!keySendComplete);
                                            keySendComplete = FALSE;
                                        }
                                        Keyboard_release(KEY_RIGHT_SHIFT);
                                        while(!keySendComplete);
                                        keySendComplete = FALSE;
                                    	joykey6 = FALSE;
                                    }
                break;
            // These cases are executed while your device is disconnected from
            // the host (meaning, not enumerated); enumerated but suspended
            // by the host, or connected to a powered hub without a USB host
            // present.
            case ST_PHYS_DISCONNECTED:
            case ST_ENUM_SUSPENDED:
            case ST_PHYS_CONNECTED_NOENUM_SUSP:
                __bis_SR_register(LPM3_bits + GIE);
                _NOP();
                break;

            // The default is executed for the momentary state
            // ST_ENUM_IN_PROGRESS.  Usually, this state only last a few
            // seconds.  Be sure not to enter LPM3 in this state; USB
            // communication is taking place here, and therefore the mode must
            // be LPM0 or active-CPU.
            case ST_ENUM_IN_PROGRESS:
            default:;
        }
    }  //while(1)
} //main()

/*  
 * ======== UNMI_ISR ========
 */
#if defined(__TI_COMPILER_VERSION__) || (__IAR_SYSTEMS_ICC__)
#pragma vector = UNMI_VECTOR
__interrupt void UNMI_ISR (void)
#elif defined(__GNUC__) && (__MSP430__)
void __attribute__ ((interrupt(UNMI_VECTOR))) UNMI_ISR (void)
#else
#error Compiler not found!
#endif
{
    switch (__even_in_range(SYSUNIV, SYSUNIV_BUSIFG))
    {
        case SYSUNIV_NONE:
            __no_operation();
            break;
        case SYSUNIV_NMIIFG:
            __no_operation();
            break;
        case SYSUNIV_OFIFG:
             UCS_clearFaultFlag(UCS_XT2OFFG);
            UCS_clearFaultFlag(UCS_DCOFFG);
            SFR_clearInterrupt(SFR_OSCILLATOR_FAULT_INTERRUPT);
            break;
        case SYSUNIV_ACCVIFG:
            __no_operation();
            break;
        case SYSUNIV_BUSIFG:
            // If the CPU accesses USB memory while the USB module is
            // suspended, a "bus error" can occur.  This generates an NMI.  If
            // USB is automatically disconnecting in your software, set a
            // breakpoint here and see if execution hits it.  See the
            // Programmer's Guide for more information.
            SYSBERRIV = 0; //clear bus error flag
            USB_disable(); //Disable
    }
}

/*
 * ======== Port1_ISR ========
 */
#if defined(__TI_COMPILER_VERSION__) || (__IAR_SYSTEMS_ICC__)
#pragma vector=PORT1_VECTOR
__interrupt void Button1_ISR (void)
#elif defined(__GNUC__) && (__MSP430__)
void __attribute__ ((interrupt(PORT1_VECTOR))) Button1_ISR (void)
#else
#error Compiler not found!
#endif
{
    switch (P1IV)
    {
    case P1IV_P1IFG1:
    	button1Pressed = TRUE;
        	__low_power_mode_off_on_exit();
        	if (!GPIO_getInputPinValue(BUTTON2_PORT, BUTTON2_PIN)) // SHIFT KEY STATEMENT
        {
        	button2Pressed = TRUE;
        }
        else {
        	button2Pressed = FALSE;
        }
    break;
    case P1IV_P1IFG3:
          button3Pressed = TRUE;
      	__low_power_mode_off_on_exit();
        /*if (! GPIO_getInputPinValue(BUTTON4_PORT, BUTTON4_PIN))
        {
        	button4Pressed = TRUE;
        }
        else {
        	button4Pressed = FALSE;
        }*/
       break;
    }
}

/*** ADC12 _ISR ***/
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = ADC12_VECTOR
__interrupt void ADC12ISR (void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(ADC12_VECTOR))) ADC12ISR (void)
#else
#error Compiler not supported!
#endif
{
    switch(__even_in_range(ADC12IV,34))
  {
  case  0: break;                           // Vector  0:  No interrupt
  case  2: break;                           // Vector  2:  ADC overflow
  case  4: break;                           // Vector  4:  ADC timing overflow
  case  6: break;                           // Vector  6:  ADC12IFG0
  case  8: break;                        	// Vector  8:  ADC12IFG1
  case 10: break;                          	// Vector 10:  ADC12IFG2
  case 12:									// Vector 12:  ADC12IFG3
	  break;
  case 14:
	  break;                           // Vector 14:  ADC12IFG4
  case 16:                           		// Vector 16:  ADC12IFG5 - ADC12IV = 0x0010
	  result0[0] = ADC12MEM3;			// move result of A3 , IFG cleared
	  if (result0[0] >= 0xfd2)
	      {
	  	    	joykey1 = TRUE;
	   	  }
	  else if (result0[0] <= 0x00f)
	   	  {
	   	    	joykey2 = TRUE;
	   	  }

	 result1[0] = ADC12MEM4;                 	// Move results of A4, IFG is cleared
		  if (result1[0] >= 0x960)
			  {
	  	    	joykey3 = TRUE;
	   	  }
	  else if (result1[0] <= 0x00f)
	   	  {
	   	    	joykey4 = TRUE;
	   	  }

	result2[0] = ADC12MEM5;               // Move results of A5, IFG is cleared
	 	 if (result2[0] >= 0x960)
	 	 {
	 		 joykey5 = TRUE;
	 	 }
	 	 else if (result2[0] <= 0x00f)
	 	 {
	 		 joykey6 = TRUE;
	 	 }
	     __low_power_mode_off_on_exit();  // exit low power mode
     //__low_power_mode_off_on_exit();  // exit low power mode
    break;
  case 18: break;                           // Vector 18:  ADC12IFG6
  case 20: break;                           // Vector 20:  ADC12IFG7
  case 22: break;                           // Vector 22:  ADC12IFG8
  case 24: break;                           // Vector 24:  ADC12IFG9
  case 26: break;                           // Vector 26:  ADC12IFG10
  case 28: break;                           // Vector 28:  ADC12IFG11
  case 30: break;                           // Vector 30:  ADC12IFG12
  case 32: break;                           // Vector 32:  ADC12IFG13
  case 34: break;                           // Vector 34:  ADC12IFG14
  default: break;
  }
}



//Released_Version_5_10_00_17

